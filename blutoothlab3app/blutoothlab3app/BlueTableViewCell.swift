//
//  BlueTableViewCell.swift
//  blutoothlab3app
//
//  Created by Christofer Berrette on 2017-11-20.
//  Copyright © 2017 Christofer Berrette. All rights reserved.
//

import UIKit

class BlueTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var rssiLabel: UILabel!
    @IBOutlet weak var uiidLable: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
